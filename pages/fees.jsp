<h3>Suggested Fees</h3>
<p><i><small>This website is neither affiliated with nor supported by the AGO (American Guild of Organists). It exists for the benefit of local
organists and churches.</small></i></p>
<p>The following fee ranges have been suggested in the past by the American Guild of Organists.  In general, the higher the expectations of
the church and/or the more complicated the service, the higher in the suggested range the actual fee should fall.</p>
<p>These fees are not binding on either the substitutes associated with this site nor on the churches using their
services.  They are provided here as a reference only.</p>

<table border="3" cellpadding="5" cellspacing="5">
<tr>
<th>Service/Rehearsal Type</th>
<th>Organist or Organist/Director</th>
<th>Fee Range</th>
<th>Notes</th>
</tr>
<tr>
<td rowspan="4">Single church or temple service</td>
<td rowspan="2">Organist only, or Director only</td>
<td>$100-$350</td>
<td>One service with brief warm-up, includes accompaniments</td>
</tr>
<tr>
<td>$50-$150/hour*</td>
<td>Separate rehearsal, depending on amount of preparation required</td>
</tr>
<tr>
<td rowspan="2">Organist/Director combination</td>
<td>$150-$500</td>
<td>One service with brief warm-up, includes accompaniments</td>
</tr>
<tr>
<td>$75-$100/hour*</td>
<td>Separate rehearsal, depending on amount of preparation required</td>
</tr>
<tr>
<td rowspan="2">Extra service(s) (contiguous in time)</td>
<td>Organist only, or Director only</td>
<td>$75-$150 each</td>
<td rowspan="2">Assumes identical services requiring no additional preparation or rehearsals</td>
</tr>
<tr>
<td>Organist/Director combination</td>
<td>$75-$200 each</td>
</tr>
<tr>
<td rowspan="3">Weddings</td>
<td rowspan="3">Organist</td>
<td>$150-$500</td>
<td>Service only (late fees apply hourly)</td>
</tr>
<tr>
<td>$50-$200/hour*</td>
<td>Rehearsal with bridal party (late fees apply hourly)</td>
</tr>
<tr>
<td>$30-$150/hour*</td>
<td>Additional rehearsals or in-person consultations</td>
</tr>
<tr>
<td rowspan="2">Funerals</td>
<td rowspan="2">Organist</td>
<td>$100-$400</td>
<td>Service with standard repertoire; assumes minimal score/repertoire research</td>
</tr>
<tr>
<td>$50-$100/hour*</td>
<td>Rehearsal - Depending on difficulty of music and amount of coordination with choir, soloists, etc.</td>
</tr>
<tr>
<td>Mileage</td>
<td colspan="3">If the round trip for a service or rehearsal exceeds 30 miles, the musician may charge
the current IRS reimbursement rate (53.5 cents per mile as of 2017) in addition to the normal fees.</td>
</tr>
</table>
* Hourly rates have a one-half-hour minimum fee
