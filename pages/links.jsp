<h3>External Links</h3>
<p>These links are provided as-is for your use. If a link no longer works, please notify the webmaster and it will either be updated or removed.</p>

<p>All links open in a new tab or window.</p>

<p><a href="http://www.musi-cal.us/calendar/calendar-list.php?F=list&ca=c116w.123&ttl=24&X=1" target="_blank">Musical Event Calendar</a></p>

<p><br>The following table contains links to regional AGO Chapters and their subbing and jobs lists (where available).</p>

<table border="3" cellpadding="5" cellspacing="5">
<tr>
<th align="right">AGO Chapter</th>
<th align="center">Main Website</th>
<th align="center">Subbing List</th>
<th align="center">Open Jobs</th>
</tr>
<tr>
<td align="right">Springfield</td>
<td align="center"><a href="http://www.springfieldago.org/" target="_blank">website</a></td>
<td align="center">You Are Here!</td>
<td align="center">N/A</td>
</tr>
<tr>
<td align="right">Worcester</td>
<td align="center"><a href="http://worcesterago.org/" target="_blank">website</a></td>
<td align="center"><a href="http://www.worcesterago.org/substitute/" target="_blank">subs</a></td>
<td align="center"><a href="http://www.worcesterago.org/jobs/" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">Boston</td>
<td align="center"><a href="http://www.bostonago.org/" target="_blank">website</a></td>
<td align="center">Private</td>
<td align="center">Private</td>
</tr>
<tr>
<td align="right">Merrimack Valley</td>
<td align="center"><a href="http://www.merrimackago.com/" target="_blank">website</a></td>
<td align="center"><a href="http://www.merrimackago.com/Home/newsletters/MVAGO%20Substitute%20list.pdf?attredirects=0&d=1" target="_blank">subs</a></td>
<td align="center"><a href="http://www.merrimackago.com/job-listings" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">Southeastern MA</td>
<td align="center"><a href="http://semassago.wixsite.com/southeasternmaago" target="_blank">website</a></td>
<td align="center"><a href="http://semassago.wixsite.com/southeasternmaago/subs" target="_blank">subs</a></td>
<td align="center"><a href="http://semassago.wixsite.com/southeasternmaago/openbenches" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">Monadnock</td>
<td align="center"><a href="http://www.monadnockago.com/" target="_blank">website</a></td>
<td align="center">N/A</td>
<td align="center">N/A</td>
</tr>
<tr>
<td align="right">Rhode Island</td>
<td align="center"><a href="http://www.riago.org/" target="_blank">website</a></td>
<td align="center"><a href="http://www.riago.org/subs.php" target="_blank">subs</a></td>
<td align="center"><a href="http://www.riago.org/benches.php" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">New Hampshire</td>
<td align="center"><a href="http://www.nhago.org/" target="_blank">website</a></td>
<td align="center"><a href="http://www.nhago.org/Substitute_List.php" target="_blank">subs</a></td>
<td align="center"><a href="http://www.nhago.org/Placement_Services.php" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">Greater Hartford</td>
<td align="center"><a href="http://www.hartfordago.org/" target="_blank">website</a></td>
<td align="center"><a href="http://www.hartfordago.org/wp-content/uploads/2013/06/SubstituteList.September2016.pdf" target="_blank">subs</a></td>
<td align="center">Private</td>
</tr>
<tr>
<td align="right">Vermont</td>
<td align="center"><a href="http://www.vtago.org/" target="_blank">website</a></td>
<td align="center"><a href="http://www.vtago.org/contact-list-for-organists/" target="_blank">subs</a></td>
<td align="center"><a href="http://www.vtago.org/other/" target="_blank">jobs</a></td>
</tr>
<tr>
<td align="right">National AGO</td>
<td align="center"><a href="http://agohq.org/" target="_blank">website</a></td>
<td align="center"><a href="https://www.agohq.org/find-a-substitute-organist/" target="_blank">subs</a></td>
<td align="center"><a href="https://www.agohq.org/careers/positions-available/" target="_blank">jobs</a></td>
</tr>
</table>

