<%@ page import="org.jmanderson.subbing.hibernate.Users"%>
<%@ page import="org.jmanderson.subbing.DataPreparer"%>
<%@ page import="org.jmanderson.subbing.DataUpdater" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>

<div id="warning2"></div>
<p />

<textarea id="xslmyprevsched" style="display: none;">
<c:import url="/XML/myschedule2.xsl" />
</textarea>
<textarea id="xsllocpieces2" style="display: none;">
<c:import url="/XML/locationpieces.xsl" />
</textarea>

<script type="text/javascript">
  var reloadMyPreviousSchedule = function() {
	  <%String myUsername = ((Users) request.getSession().getAttribute("user")).getUsername();%>
	  DataPreparer.getOrganistScheduleAsXML(true, "<%=myUsername%>", displayMyPreviousSchedule);
	}
	var displayMyPreviousSchedule = function(xmlIn) {
		$('#myPreviousSchedule').xslt(xmlIn, $('#xslmyprevsched').text());
	}
	var checkCalendar2 = function() {
		require(["dojo/dom"], function(dom) {
		var showInCal;
		var myUsername = "<%=myUsername%>";
		DataPreparer.getMyShowInCal(myUsername, { async:false, callback:function(str) {
			if (str == "false") {
				dom.byId('warning2').innerHTML = "<center><font color='red' size='+1'><i>Your availability is not visible from the public &quot;Calendar&quot; tab.<br />To correct this, go to the &quot;[My Info]&quot; tab and check the &quot;Show in Calendar&quot; checkbox.</i></font></center>";
			}
			else {
				dom.byId('warning2').innerHTML = " ";
			}
		}});
		});
	}
	var showLocationPieces2 = function(locID) {
		require(["dojo/dom"], function(dom) {
		DataPreparer.getLocationDisplay(locID, {callback:function(str) { dom.byId('piecesLocation2').innerHTML = str;}});
		var myUsername = "<%=myUsername%>";
		DataPreparer.getLocationPiecesAsXml(myUsername, locID, {callback:function(xmlIn) {
			$('#locationPieces2').xslt(xmlIn, $('#xsllocpieces2').text());
		}});
		dijit.byId("piecesPlayed2").show();
		});
	}
	var showDateDialog2 = function(month, day, holiday) {
		require(["dojo/dom"], function(dom) {
		var m, y, u, t, id;
		dom.byId("ddDay1p").innerHTML = day;
		dom.byId("ddHoliday1p").innerHTML = holiday;
		DateHelper.extractMonthFromDisplay(month, {async:false, callback:function(str) {m=str;}});
		DateHelper.extractYearFromDisplay(month, {async:false, callback:function(str) {y=str;}});
		dom.byId("ddM1p").innerHTML = m;
		dom.byId("ddY1p").innerHTML = y;
		var ddd = "<b>Schedule for " + day + " " + month + "</b><p/>";
		dom.byId("ddDate1p").innerHTML = ddd;
		DataPreparer.getLocationsForForm("<%=myUsername%>", "locationList1p", {async:false, callback:function(str) {
			dom.byId("ddLocations1p").innerHTML = str;
		}});
		DataPreparer.getLocationsForForm("<%=myUsername%>", "locationList2p", {async:false, callback:function(str) {
			dom.byId("ddLocations2p").innerHTML = str;
		}});
		DataPreparer.getScheduleID(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			id = str;
		}});
		dom.byId("ddID1p").innerHTML = id;
		if (id == 0) {
			dijit.byId("ddUnavailable1p").set("checked",false);
			dijit.byId("ddTentative1p").set("checked", false);
			dom.byId("ddNotes1p").value = "";
			dom.byId("ddPieces1p").value = "";
			dom.byId("ddTime1p").value = "";
			dom.byId("ddNotes2p").value = "";
			dom.byId("ddPieces2p").value = "";
			dom.byId("ddTime2p").value = "";
		}
		else {
		DataPreparer.getScheduleLocationID(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("locationList1p").value = str;
		}});
		DataPreparer.getScheduleLocationID2(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("locationList2p").value = str;
		}});
		DataPreparer.getScheduleUnavailable(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			u = str;
		}});
		var ubool = true;
		if (u == "false") {
			ubool = false;
		}
		dijit.byId("ddUnavailable1p").set("checked", ubool);
		DataPreparer.getScheduleTentative(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			t = str;
		}});
		var tbool = true;
		if (t == "false") {
			tbool = false;
		}
		dijit.byId("ddTentative1p").set("checked", tbool);
		DataPreparer.getScheduleNotes(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddNotes1p").value = str;
		}});
		DataPreparer.getScheduleNotes2(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddNotes2p").value = str;
		}});
		DataPreparer.getSchedulePieces(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddPieces1p").value = str;
		}});
		DataPreparer.getSchedulePieces2(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddPieces2p").value = str;
		}});
		DataPreparer.getScheduleTime(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddTime1p").value = str;
		}});
		DataPreparer.getScheduleTime2(y, m, day, "<%=myUsername%>", holiday, {async:false, callback:function(str) {
			dom.byId("ddTime2p").value = str;
		}});
		}
		dijit.byId("dateDialog2").show();
		});
	}
	var submitDate2 = function() {
		require(["dojo/dom"], function(dom) {
		var id = dom.byId("ddID1p").innerHTML;
		var year = dom.byId("ddY1p").innerHTML;
		var month = dom.byId("ddM1p").innerHTML;
		var day = dom.byId("ddDay1p").innerHTML;
		var holiday = dom.byId("ddHoliday1p").innerHTML;
		var locID = dom.byId("locationList1p").value;
		var notes = dom.byId("ddNotes1p").value;
		var pieces = dom.byId("ddPieces1p").value;
		var time = dom.byId("ddTime1p").value;
		var locID2 = dom.byId("locationList2p").value;
		var notes2 = dom.byId("ddNotes2p").value;
		var pieces2 = dom.byId("ddPieces2p").value;
		var time2 = dom.byId("ddTime2p").value;
		var tentative = dijit.byId("ddTentative1p").get("checked");
		var unavailable = dijit.byId("ddUnavailable1p").get("checked");
		DataUpdater.addOrUpdateSchedule("<%=myUsername%>", id, year, month, day, locID, notes, pieces, time,
				tentative, holiday, unavailable, false, locID2, notes2, pieces2, time2, {async:false, callback:function(){
					reloadMyPreviousSchedule();
					reloadCalendar();
				}});
		});
	}
	var deleteSchedule2 = function(id) {
		DataUpdater.deleteSchedule(id, {async:false, callback:function() {
			reloadMyPreviousSchedule();
			reloadCalendar();
		}});
	}
</script>
<p><center><font color="blue"><i>
  <b>&raquo;</b>&nbsp;&nbsp;Click on a date to update the schedule for that day and the pieces played<br/>
  <b>&raquo;</b>&nbsp;&nbsp;Hover on the <img src="images/checkmark3.gif" /> to view the pieces played that day<br/>
  <b>&raquo;</b>&nbsp;&nbsp;Click on a location name to see all pieces ever played at that location<br/>
  <b>&raquo;</b>&nbsp;&nbsp;Click the <img src="images/delete_sm.png" /> to delete that day&#39;s schedule<br/>
</i></font></center></p>

<div data-dojo-type="dijit.Dialog" id="dateDialog2" title="Edit Schedule" execute="submitDate2();">
<center><div id="ddDate1p"></div></center>
<table>
<tr><td colspan="2"><hr/></td></tr><tr>
<th rowspan="2" align="right">For this Date:</th>
<td><input data-dojo-type="dijit.form.CheckBox" name="ddUnavailable1p" id="ddUnavailable1p">&nbsp;Check here to mark yourself UNAVAILABLE</td></tr>
<tr><td><input data-dojo-type="dijit.form.CheckBox" name="ddTentative1p" id="ddTentative1p">&nbsp;Check here to mark this date TENTATIVE</td></tr>
<td colspan="2"><hr/></td></tr>
<tr><th align="right">Primary Location:</th>
<td><div id="ddLocations1p"></div></td>
</tr><tr><th align="right">Time:</th><td><input data-dojo-type="dijit.form.TextBox" type="text" name="ddTime1p" id="ddTime1p"/></td></tr>
<tr>
			<th align="right" valign="top">
				Notes:
			</th>
			<td>
				<input data-dojo-type="dijit.form.Textarea" type="text" name="ddNotes1p"
					id="ddNotes1p" style="width: 400px;">
				<span data-dojo-type="dijit.Tooltip"
					data-dojo-props="connectId:'ddNotes1p'">Use this field for any information you want to capture
					relating to the service, rehearsals and times, etc.</span>
					<div id="ddM1p" style="display:none;"></div><div id="ddY1p" style="display:none;"></div>
					<div id="ddID1p" style="display:none;"></div><div id="ddDay1p" style="display:none;"></div>
					<div id="ddHoliday1p" style="display:none;"></div>
			</td>
		</tr>
		<tr>
			<th align="right" valign="top">
				Pieces Played:
			</th>
			<td>
				<input data-dojo-type="dijit.form.Textarea" type="text"
					name="ddPieces1p" id="ddPieces1p" style="width: 400px">
				<span data-dojo-type="dijit.Tooltip"
					data-dojo-props="connectId:'ddPieces1p'">Entering pieces played here allows you to take advantage
					of other functionality on this site such as seeing all the pieces you have played at this Location,
					what you played on any given date, etc.</span>
			</td>
		</tr>
		<tr><td colspan="2"><hr/></td></tr>
<tr><th align="right">(Optional) Second Location:</th>
<td><div id="ddLocations2p"></div></td>
</tr><tr><th align="right">Time:</th><td><input data-dojo-type="dijit.form.TextBox" type="text" name="ddTime2p" id="ddTime2p"/></td></tr>
<tr>
			<th align="right" valign="top">
				Notes:
			</th>
			<td>
				<input data-dojo-type="dijit.form.Textarea" type="text" name="ddNotes2p"
					id="ddNotes2p" style="width: 400px;">
				<span data-dojo-type="dijit.Tooltip"
					data-dojo-props="connectId:'ddNotes2p'">Use this field for any information you want to capture
					relating to the service, rehearsals and times, etc.</span>
			</td>
		</tr>
		<tr>
			<th align="right" valign="top">
				Pieces Played:
			</th>
			<td>
				<input data-dojo-type="dijit.form.Textarea" type="text"
					name="ddPieces2p" id="ddPieces2p" style="width: 400px">
				<span data-dojo-type="dijit.Tooltip"
					data-dojo-props="connectId:'ddPieces2p'">Entering pieces played here allows you to take advantage
					of other functionality on this site such as seeing all the pieces you have played at this Location,
					what you played on any given date, etc.</span>
			</td>
		</tr>
		<tr><td colspan="2"><hr/></td></tr>
		<tr>
			<td align="center" colspan="2">
				<button data-dojo-type="dijit.form.Button" type="submit">
					<b>Update Schedule</b>
				</button>
				<button data-dojo-type="dijit.form.Button" type="button"
					onClick="dijit.byId('dateDialog2').hide();">
					<i>Cancel</i>
				</button>
			</td>
		</tr>
</table>
</div>
<div data-dojo-type="dijit.Dialog" id="piecesPlayed2"
	title="Pieces Played">
<center><em><div id="piecesLocation2"></div></em></center>
<div id="locationPieces2"></div>	
<center>
				<button data-dojo-type="dijit.form.Button" type="button"
					onClick="dijit.byId('piecesPlayed2').hide();">
					<b>Close Window</b>
				</button></center>
</div>
	

 
<div id="myPreviousSchedule"></div>
